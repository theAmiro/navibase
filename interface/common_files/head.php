<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">

<title>Navibase | <?= $page_title;?></title>

<!-- Bootstrap Core CSS -->
<link href="external_libraries/bootstrap/css/bootstrap.min.css"
	rel="stylesheet">

<!-- Datatables Core CSS -->
<link
	href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css"
	rel="stylesheet">
<link
	href="external_libraries/datatables/css/dataTables.bootstrap.min.css"
	rel="stylesheet">

<!-- Custom CSS -->
<link href="internal_libraries/css/custom.css" rel="stylesheet">

<!-- Carousel Slider CSS -->
<link href="internal_libraries/css/js-image-slider.css" rel="stylesheet">

<!-- Font Awesome -->
<link rel="stylesheet"
	href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css">
<link rel="stylesheet"
	href="external_libraries/font-awesome/css/font-awesome.css">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
<!-- Active Navigation Item -->
<?php $activePage = basename ( $_SERVER ['PHP_SELF'], ".php" );?>