<!-- jQuery Version 1.11.1 -->
<script src="external_libraries/bootstrap/js/jquery.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="external_libraries/bootstrap/js/bootstrap.min.js"></script>

<!-- Datatables JQuery -->
<script
	src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>

<script
	src="external_libraries/datatables/js/dataTables.bootstrap.min.js"></script>

<!-- Datatables JavaScript -->
<script
	src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>

<!-- Carousel Slider Javascript -->
<script src="internal_libraries/js/js-image-slider.js"></script>

<!-- Custom Javascript -->
<script src="internal_libraries/js/custom.js"></script>


