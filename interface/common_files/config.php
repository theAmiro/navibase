<?php
session_start ();

$_SESSION ["connection"] ["host"] = "localhost";
$_SESSION ["connection"] ["username"] = "root";
$_SESSION ["connection"] ["password"] = "";
$_SESSION ["connection"] ["dbname"] = "navigators";
function build_connection_string() {
	$connection_string = "host=" . $_SESSION ["connection"] ["host"] . " ";
	$connection_string .= "username=" . $_SESSION ["connection"] ["username"] . " ";
	$connection_string .= "password=" . $_SESSION ["connection"] ["password"] . "";
	$connection_string .= "dbname=" . $_SESSION ["connection"] ["dbname"] . " ";
	return $connection_string;
}
$_SESSION["connection"]["string"] = build_connection_string();
?>