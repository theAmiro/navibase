<?php
require_once ("session.php");
include_once ("../classes/Users.php");

$new_user = new Users ();
$username = $_SESSION ['user_session'];

$query = $new_user->execute_query ( "SELECT * FROM users where username = :username" );

$query->execute ( array (
		":username" => $username 
) );
$userRow = $query->fetch ( PDO::FETCH_ASSOC );

if (isset ( $_POST ['registerbtn'] )) {
	$username = strip_tags ( $_POST ['txtusername'] );
	$email = strip_tags ( $_POST ['txtemail'] );
	$password = strip_tags ( $_POST ['txtpassword'] );
	$confirm_password = strip_tags ( $_POST ['confirm_txtpassword'] );
	$added_by = strip_tags ( $_SESSION ['user_session'] );
	
	if ($username == "") {
		$error [] = "Username Cannot be empty!";
	} elseif ($email == "") {
		$error [] = "Email Cannot be empty!";
	} elseif (! filter_var ( $email, FILTER_VALIDATE_EMAIL )) {
		$error [] = "Please enter a valid email address!";
	} elseif ($password == "") {
		$error [] = "Password Cannot be empty!";
	} elseif (strlen ( $password ) < 8) {
		$error [] = "Password Must contain more than 8 Characters!";
	} elseif ($password != $confirm_password) {
		$error [] = "Passwords don't match!";
	} else {
		try {
			$query = $new_user->execute_query ( "SELECT * FROM users WHERE username=:username OR email=:email" );
			$query->execute ( array (
					':username' => $username,
					':email' => $email 
			) );
			$row = $query->fetch ( PDO::FETCH_ASSOC );
			
			if ($row ['username'] == $username) {
				$error [] = "Username already taken!";
			} elseif ($row ['email'] == $email) {
				$error [] = "Email already registered!";
			} else {
				if ($new_user->register ( $username, $email, $password, $added_by )) {
					$new_user->redirect ( 'users.php?joined' );
				}
			}
		} catch ( PDOException $e ) {
			echo $e->getMessage ();
		}
	}
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
<?php
$page_title = "Users";
include_once ("common_files/head.php");
?>
</head>
<body>
<?php include_once("common_files/nav.php");?>
	<!-- Page Content -->
	<div class="container">
		<ul class="nav nav-tabs" role="tablist">
			<li class="nav-item active" role="presentation"><a class="nav-link"
				data-toggle="tab" href="#addition_form" role="tab"> <span><i
						class="fa fa-plus"></i></span> Add Users
			</a></li>
			<li class="nav-item" role="presentation"><a class="nav-link"
				data-toggle="tab" href="#listing" role="tab"> <span><i
						class="fa fa-list"></i></span> Listing <span class="badge">
						<?php
						include_once ("../classes/Users.php");
						$current_users = new Users ();
						$current_list = $current_users->getList ();
						echo count ( $current_list );
						?></span>
			</a></li>
		</ul>
		<!-- Tab Panes -->
		<div class="tab-content">
			<div class="tab-pane fade in active" id="addition_form"
				role="tabpanel">
				<div class="row">
					<div class="col-lg-12">
						<div class="panel panel-primary">
							<div class="panel-heading">
								<span class="pull-right"><i class="fa fa-user-plus fa-3x"></i></span>
								<h4>NEW USER</h4>
							</div>
							<form method="post">
								<div class="panel-body">
									<div class="row">
										<div class="col-md-6 col-md-offset-3">
											<div class="row">
												<div class="col-lg-6">
													<h4>User Information</h4>
												</div>
											</div>
											<div class="row">
												<div class="col-md-12">
												<?php
												if (isset ( $error )) {
													foreach ( $error as $error ) {
														?>
													<div class="alert alert-danger" role="alert" id="messages">
														<p>
														<?= $error;?>
														<span class="pull-right"><i
																class="fa fa-times-circle fa-lg"></i></span>
														</p>
													</div>
													<?php } }elseif (isset($_GET['joined'])){?>
													<div class="alert alert-success" role="alert" id="messages">
														<p>
															User has successfully been added! <span
																class="pull-right"><i class="fa fa-tick-circle fa-lg"></i></span>
														</p>
													</div>
													<?php }?>
													<div class="input-group">
														<span class="input-group-addon"><i class="fa fa-user"></i></span>
														<input type="text" class="form-control"
															value="<?php if (isset($error)){ echo $username;}?>"
															placeholder="Username" aria-describedby="basic-addon1"
															required autofocus name="txtusername" id="txtusername">
													</div>
													<span id="passblock" class="help-block">Enter Password to
														effect changes</span> <br>
													<div class="input-group">
														<span class="input-group-addon"><i class="fa fa-envelope"></i></span>
														<input type="text" class="form-control"
															value="<?php if (isset($error)){ echo $email;}?>"
															placeholder="Email" aria-describedby="basic-addon1"
															required name="txtemail" id="txtemail">
													</div>
													<br>
													<div class="input-group">
														<span class="input-group-addon"><i class="fa fa-lock"></i></span>
														<input type="password" class="form-control"
															placeholder="Password" aria-describedby="basic-addon1"
															required name="txtpassword" id="txtpassword">
													</div>
													<br>
													<div class="input-group">
														<span class="input-group-addon"><i class="fa fa-lock"></i></span>
														<input type="password" class="form-control"
															placeholder="Confirm Password"
															aria-describedby="basic-addon1" required
															name="confirm_txtpassword" id="confirm_txtpassword">
													</div>
													<br>
													<div class="row">
														<div class="col-md-6 col-xs-6" align="left"></div>
														<div class="col-md-6 col-xs-6" align="right">
															<button class="btn btn-success" type="submit"
																name="registerbtn">
																<i class="fa fa-user-plus fa-lg"></i> Register
															</button>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
				<!-- /.row -->
			</div>
			<div class="tab-pane fade" id="listing" role="tabpanel">
				<div class="row">
					<div class="col-lg-12">
						<div class="panel panel-primary">
							<div class="panel-heading clearfix">
								<span class="pull-right"><i class="fa fa-users fa-3x"></i></span>
								<h4>USERS LISTING</h4>
							</div>
							<div class="panel-body">
								<div class="table-responsive">
									<table id="datatable"
										class="table table-striped table-condensed table-bordered">
										<thead>
											<tr>
												<th>ID</th>
												<th>Username</th>
												<th>Email</th>
												<th>Added By</th>
												<th>Registration Date</th>
												<th>Action</th>
											</tr>
										</thead>
										<tbody>
							<?php
							include_once ("../classes/Users.php");
							$current_users = new Users ();
							$current_list = $current_users->getList ();
							if (sizeof ( $current_list ) > 0) {
								foreach ( $current_list as $current_item ) {
									?>
											<tr>
												<td><?php print $current_item['id'];?></td>
												<td><?php print $current_item['username'];?></td>
												<td><?php print $current_item['email'];?></td>
												<td><?php print $current_item['added_by'];?></td>
												<td><?php print $current_item['date'];?></td>
												<td align="center"><button type="button"
														class="btn btn-danger btn-sm">Delete</button></td>
											</tr>
							<?php }}else {?>
											<tr class="danger">
												<td colspan="4" align="center"><strong>No Entries Found</strong></td>
											</tr>
							<?php }?>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- /.container -->
<?php include_once ("common_files/javascript.php");?>
</body>
</html>
