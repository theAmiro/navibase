<?php
session_start ();
include_once ("../classes/Users.php");
$session = new Users ();

if (! $session->is_Loggedin ()) {
	// session no set redirects to login page
	$session->redirect ( 'index.php' );
}
?>