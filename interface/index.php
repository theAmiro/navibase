<?php
session_start ();
include_once ("../classes/Users.php");
$login = new Users ();
if ($login->is_Loggedin ()) {
	$login->redirect ( "members.php" );
} else {
	$expire = "You must log in to continue!";
}

if (isset ( $_POST ['loginbtn'] )) {
	$username = strip_tags ( $_POST ['txtusername_email'] );
	$email = strip_tags ( $_POST ['txtusername_email'] );
	$password = strip_tags ( $_POST ['txtpassword'] );
	
	if ($login->login ( $username, $email, $password )) {
		$login->redirect ( "members.php" );
	} else {
		$error = "Wrong username-password combination!";
	}
}
?>
<!DOCTYPE html>
<html lang="en">
<?php
$page_title = "Login";
include_once ("common_files/head.php");
?>
<body>
	<div class="container">
		<div class="row">
			<div class="col-md-8 hidden-md hidden-sm hidden-xs">
				<div class="" id="sliderFrame">
					<div id="slider">
						<img src="images/navslide1.png" alt="" /> <img
							src="images/navslide2.png" alt="" /> <img
							src="images/navslide1.png" alt="" /><img
							src="images/navslide2.png" alt="" />
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<?php
				if (isset ( $error )) {
					?>
				<div class="alert alert-danger" role="alert" id="messages">
					<span class="glyphicon glyphicon-exclamation-sign"
						aria-hidden="true"></span> <span class="sr-only">Error:</span>
					<?= $error;?>
				</div>
				<?php
				}
				?>
				<h2>Sign in</h2>
				<form method="post" action="">
					<div class="input-group">
						<span class="input-group-addon"><i class="fa fa-user"></i></span>
						<input type="text" class="form-control" name="txtusername_email"
							value="<?php if (isset($error)){ echo $username;}?>"
							placeholder="Username" aria-describedby="basic-addon1" required
							autofocus>
					</div>
					<br>
					<div class="input-group">
						<span class="input-group-addon" aria-hidden="true"><i
							class="fa fa-lock"></i></span> <input type="password"
							class="form-control" name="txtpassword" placeholder="Password"
							aria-describedby="basic-addon1" required>
					</div>
					<br>
					<button class="btn btn-lg btn-primary btn-block" type="submit"
						name="loginbtn">Log in</button>
				</form>
			</div>
		</div>
	</div>
	<!-- /container -->
	<?php include_once ("common_files/javascript.php");?>
</body>
</html>
