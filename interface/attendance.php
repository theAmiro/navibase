<?php
require_once ("session.php");
include_once ("../classes/Users.php");

$authorize_user = new Users ();

$username = $_SESSION ['user_session'];

$query = $authorize_user->execute_query ( "SELECT * FROM users where username = :username" );

$query->execute ( array (
		":username" => $username 
) );
$userRow = $query->fetch ( PDO::FETCH_ASSOC );
?>
<!DOCTYPE html>
<html lang="en">
<?php
$page_title = "Attendance";
include_once ("common_files/head.php");
?>
<body>
	<?php include_once ("common_files/nav.php");?>
	<!-- Page Content -->
	<div class="container">
		<ul class="nav nav-tabs" role="tablist">
			<li class="nav-item active" role="presentation"><a class="nav-link"
				data-toggle="tab" href="#attendance" role="tab"> <span><i
						class="fa fa-check"></i></span> Attendance Register
			</a></li>
			<li class="nav-item" role="presentation"><a class="nav-link"
				data-toggle="tab" href="#summary" role="tab"> <span><i
						class="fa fa-list"></i></span> Summary
			</a></li>
		</ul>
		<!-- Tab panes -->
		<div class="tab-content">
			<div class="tab-pane fade  in active" id="attendance" role="tabpanel">
				<div class="row">
					<div class="col-lg-12">
						<div class="panel panel-primary">
							<div class="panel-heading clearfix">
								<span class="pull-right"><i class="fa fa-check fa-3x"></i></span>
								<h4>ATTENDANCE REGISTER</h4>
							</div>
							<div class="panel-body">
								<div class="table-responsive">
									<table
										class="table table-striped table-condensed table-bordered"
										id="datatable">
										<thead>
											<tr>
												<th>ID</th>
												<th>Name</th>
												<th>Level</th>
												<th>Group</th>
												<th>Date</th>
												<th>Attendance</th>
											</tr>
										</thead>
										<tbody>
										<?php
										include_once ("../classes/Members.php");
										$current_members = new Members ();
										$current_list = $current_members->getList ();
										if (sizeof ( $current_list ) > 0) {
											foreach ( $current_list as $current_item ) {
												?>
											<tr>
												<td><?php print $current_item['id'];?></td>
												<td><?php print $current_item['name'];?></td>
												<td><?php print $current_item['level'];?></td>
												<td><?php print $current_item['group_name'];?></td>
												<td><?php print date("d-M-Y")?></td>
												<td align="center"><input type="checkbox"></td>
											</tr>
										<?php }}else {?>
											<tr>
												<td colspan="6"><?php print $current_item['id'];?></td>
											</tr>
										<?php
										}
										?>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="tab-pane fade" id="summary" role="tabpanel">
				<div class="row">
					<div class="col-lg-12">
						<div class="panel panel-primary">
							<div class="panel-heading clearfix">
								<span class="pull-right"><i class="fa fa-refresh fa-3x"></i></span>
								<h4>UPDATE GROUP LISTING</h4>
							</div>
							<div class="panel-body">
								<div class="table-responsive">
									<table id="tabledata"
										class="table table-striped table-bordered">
										<thead>
											<tr>
												<th>ID</th>
												<th>Firstname</th>
												<th>Lastname</th>
												<th>Email</th>
												<th colspan="2">Function</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td>1</td>
												<td>John</td>
												<td>Doe</td>
												<td>john@example.com</td>
												<td align="center"><button class="btn btn-success">
														<span><i class="fa fa-refresh"></i></span> Update
													</button></td>
												<td align="center"><button class="btn btn-danger">
														<span><i class="fa fa-trash"></i></span> Delete
													</button></td>
											</tr>
											<tr>
												<td>2</td>
												<td>Mary</td>
												<td>Moe</td>
												<td>mary@example.com</td>
												<td align="center"><button class="btn btn-success">
														<span><i class="fa fa-refresh"></i></span> Update
													</button></td>
												<td align="center"><button class="btn btn-danger">
														<span><i class="fa fa-trash"></i></span> Delete
													</button></td>
											</tr>
											<tr>
												<td>3</td>
												<td>July</td>
												<td>Dooley</td>
												<td>july@example.com</td>
												<td align="center"><button class="btn btn-success">
														<span><i class="fa fa-refresh"></i></span> Update
													</button></td>
												<td align="center"><button class="btn btn-danger">
														<span><i class="fa fa-trash"></i></span> Delete
													</button></td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

		</div>
		<!-- /.row -->
	</div>
	<!-- /.container -->
	<?php
	include_once ("common_files/javascript.php");
	?>
</body>
</html>