<?php
require_once ("session.php");
include_once ("../classes/Groups.php");
include_once ("../classes/Users.php");

$authorize_user = new Users ();

$username = $_SESSION ['user_session'];

$query = $authorize_user->execute_query ( "SELECT * FROM users where username = :username" );

$query->execute ( array (
		":username" => $username 
) );
$userRow = $query->fetch ( PDO::FETCH_ASSOC );

$new_group = new Groups ();
if (isset ( $_POST ['registerbtn'] )) {
	$name = strip_tags ( $_POST ['txtname'] );
	$description = strip_tags ( $_POST ['txtdescription'] );
	$level = strip_tags ( $_POST ['txtlevel'] );
	$overseer = strip_tags ( $_POST ['txtoverseer'] );
	$year = strip_tags ( $_POST ['txtyear'] );
	
	if ($name == "") {
		$error [] = "The Group Name cannot be empty!";
	} elseif ($level == "") {
		$error [] = "The Group Level cannot be empty!";
	} elseif ($overseer == "") {
		$error [] = "The Overseer name cannot be empty!";
	} elseif ($year == "") {
		$error [] = "The Year cannot be empty!";
	} else {
		try {
			$query = $new_group->execute_query ( "SELECT * FROM groups WHERE name=:name" );
			$query->execute ( array (
					':name' => $name 
			) );
			$row = $query->fetch ( PDO::FETCH_ASSOC );
			if ($row ['name'] == $name) {
				$error [] = "A group by the name of <b>" . $name . "</b> already exists!";
			} else {
				if ($new_group->register ( $name, $description, $level, $overseer, $year )) {
					$new_group->redirect ( 'groups.php?added' );
				}
			}
		} catch ( PDOException $e ) {
			echo $e->getMessage ();
		}
	}
}
?>
<!DOCTYPE html>
<html lang="en">
<?php
$page_title = "Groups";
include_once ("common_files/head.php");
?>
<body>
	<?php include_once ("common_files/nav.php");?>
	<!-- Page Content -->
	<div class="container">
		<ul class="nav nav-tabs" role="tablist">
			<li class="nav-item active" role="presentation"><a class="nav-link"
				data-toggle="tab" href="#add_groups" role="tab"> <span><i
						class="fa fa-plus"></i></span> Add Groups
			</a></li>
			<li class="nav-item" role="presentation"><a class="nav-link"
				data-toggle="tab" href="#groups" role="tab"> <span><i
						class="fa fa-list"></i></span> Listing <span class="badge">
						<?php
						include_once ("../classes/Groups.php");
						$current_groups = new Groups ();
						$current_list = $current_groups->getList ();
						echo count ( $current_list );
						?></span>
			</a></li>
		</ul>
		<!-- Tab panes -->
		<div class="tab-content">
			<div class="tab-pane fade  in active" id="add_groups" role="tabpanel">
				<div class="row">
					<div class="col-lg-12">
						<div class="panel panel-primary">
							<div class="panel-heading clearfix">
								<span class="pull-right"><i class="fa fa-users fa-3x"></i></span>
								<h4>ADD NEW GROUP</h4>
							</div>
							<form method="POST">
								<div class="panel-body">
									<div class="row">
										<div class="col-md-6"><?php
										if (isset ( $error )) {
											foreach ( $error as $error ) {
												?>
													<div class="alert alert-danger" role="alert" id="messages">
												<p>
														<?= $error;?>
														<span class="pull-right"><i
														class="fa fa-times-circle fa-lg"></i></span>
												</p>
											</div>
													<?php } }elseif (isset($_GET['added'])){?>
													<div class="alert alert-success" role="alert" id="messages">
												<p>
													Group has successfully been added! <span class="pull-right"><i
														class="fa fa-tick-circle fa-lg"></i></span>
												</p>
											</div>
													<?php }?>
										</div>
									</div>
									<div class="row">
										<div class="col-md-6">
											<label>Group Name:</label>
											<div class="input-group">
												<span class="input-group-addon"><i class="fa fa-pencil"></i></span>
												<input type="text" class="form-control"
													value="<?php if (isset($error)){ echo $name;}?>"
													placeholder="Group Name" aria-describedby="basic-addon1"
													required autofocus name="txtname" id="txtname">
											</div>
											<br> <label>Description:</label>
											<div class="input-group">
												<span class="input-group-addon"><i class="fa fa-comment"></i></span>
												<textarea class="form-control" rows="3" cols=""
													name="txtdescription"><?php if (isset($error)){ echo $description;}?></textarea>
											</div>
											<br> <label>Group Level:</label>

											<div class="input-group">
												<span class="input-group-addon"><i class="fa fa-signal"></i></span>
												<select class="form-control" name="txtlevel" id="txtlevel">
													<option selected="selected" style="display: none;">Select
														Level</option>
												<?php
												include_once ("../classes/Levels.php");
												$current_levels = new Levels ();
												$current_list = $current_levels->getList ();
												if (sizeof ( $current_list ) > 0) {
													foreach ( $current_list as $current_item ) {
														?>
													<option><?php print $current_item['name'];?></option>
													<?php }}?>
												</select>
											</div>
											<br>
										</div>
										<div class="col-md-6">
											<label>Overseer:</label>
											<div class="input-group">
												<span class="input-group-addon"><i class="fa fa-user"></i></span>
												<input type="text" class="form-control"
													value="<?php if (isset($error)){ echo $overseer;}?>"
													placeholder="Overseer" aria-describedby="basic-addon1"
													required autofocus name="txtoverseer" id="txtoverseer">
											</div>
											<br> <label>Year:</label>
											<div class="input-group">
												<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
												<input type="text" class="form-control"
													value="<?php if (isset($error)){ echo $year;}?>"
													placeholder="Year" aria-describedby="basic-addon1" required
													autofocus name="txtyear" id="txtyear">
											</div>
											<br>
										</div>
									</div>
									<div class="row">
										<div class="col-md-6 col-xs-6" align="left"></div>
										<div class="col-md-6 col-xs-6" align="right">
											<button type="submit" name="registerbtn"
												class="btn btn-success">
												<i class="fa fa-save fa-lg"></i> Save
											</button>
										</div>
									</div>

								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
			<div class="tab-pane fade" id="groups" role="tabpanel">
				<div class="row">
					<div class="col-lg-12">
						<div class="panel panel-primary">
							<div class="panel-heading">
								<span class="pull-right"><i class="fa fa-group fa-3x"></i></span>
								<h4>GROUP LISTINGS</h4>
							</div>
							<div class="panel-body">
								<div class="row">
									<?php
									include_once ("../classes/Groups.php");
									$current_groups = new Groups ();
									$current_list = $current_groups->getList ();
									if (sizeof ( $current_list ) > 0) {
										foreach ( $current_list as $current_item ) {
											?>
									<div class="col-md-4">
										<div class="panel panel-primary">
											<div class="panel-heading">
												<span class="pull-right"><?php print $current_item['year'];?></span>
												<?php print $current_item['name'];?></div>
											<ul class="list-group">
												<li class="list-group-item list-group-item-success">
												<?php print $current_item['overseer'];?></li>
												<li class="list-group-item">Dapibus ac facilisis in</li>
											</ul>
										</div>
									</div>
									<?php }}else{?>
									<div class="col-md-12">
										<div class="panel panel-default">
											<div class="panel-heading">No Entries Found!</div>
										</div>
									</div>
									<?php }?>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- /.row -->
			</div>
			<div class="tab-pane fade" id="update" role="tabpanel">
				<div class="row">
					<div class="col-lg-12">
						<div class="panel panel-primary">
							<div class="panel-heading clearfix">
								<span class="pull-right"><i class="fa fa-refresh fa-3x"></i></span>
								<h4>UPDATE GROUP LISTING</h4>
							</div>
							<div class="panel-body">
								<div class="table-responsive">
									<table id="tabledata"
										class="table table-striped table-bordered">
										<thead>
											<tr>
												<th>ID</th>
												<th>Firstname</th>
												<th>Lastname</th>
												<th>Email</th>
												<th colspan="2">Function</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td>1</td>
												<td>John</td>
												<td>Doe</td>
												<td>john@example.com</td>
												<td align="center"><button class="btn btn-success">
														<span><i class="fa fa-refresh"></i></span> Update
													</button></td>
												<td align="center"><button class="btn btn-danger">
														<span><i class="fa fa-trash"></i></span> Delete
													</button></td>
											</tr>
											<tr>
												<td>2</td>
												<td>Mary</td>
												<td>Moe</td>
												<td>mary@example.com</td>
												<td align="center"><button class="btn btn-success">
														<span><i class="fa fa-refresh"></i></span> Update
													</button></td>
												<td align="center"><button class="btn btn-danger">
														<span><i class="fa fa-trash"></i></span> Delete
													</button></td>
											</tr>
											<tr>
												<td>3</td>
												<td>July</td>
												<td>Dooley</td>
												<td>july@example.com</td>
												<td align="center"><button class="btn btn-success">
														<span><i class="fa fa-refresh"></i></span> Update
													</button></td>
												<td align="center"><button class="btn btn-danger">
														<span><i class="fa fa-trash"></i></span> Delete
													</button></td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- /.container -->
	<?php
	include_once ("common_files/javascript.php");
	?>
</body>
</html>
