<!DOCTYPE html>
<html lang="en">
<?php
$page_title = "Mailing";
include_once ("common_files/head.php");
?>
<body>
	<?php include_once ("common_files/nav.php");?>
	<!-- Page Content -->
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-primary">
					<div class="panel-heading clearfix">
						<span class="pull-right"><i class="fa fa-envelope fa-3x"></i></span>
						<h4>Mass Mailing</h4>
					</div>
					<div class="panel-body">
						<div class="col-lg-12">
							<form action="" method="post">
								<label>Receipients:</label><br>
								<div class="row" align="center">
									<div class="col-md-2" aria-hidden="true" data-toggle="buttons">
										<label class="btn btn-primary check"> <input type="checkbox"
											checked autocomplete="off"> Expo
										</label>
									</div>
									<div class="col-md-2" aria-hidden="true" data-toggle="buttons">
										<label class="btn btn-primary check"> <input type="checkbox"
											checked autocomplete="off"> Coregroupers
										</label>
									</div>
									<div class="col-md-2" aria-hidden="true" data-toggle="buttons">
										<label class="btn btn-primary check"> <input type="checkbox"
											checked autocomplete="off"> Operation Timothy
										</label><br>
									</div>
									<div class="col-md-2" aria-hidden="true" data-toggle="buttons">
										<label class="btn btn-primary check"> <input type="checkbox"
											checked autocomplete="off"> Overseers
										</label><br>
									</div>
									<div class="col-md-2" aria-hidden="true" data-toggle="buttons">
										<label class="btn btn-primary check"> <input type="checkbox"
											checked autocomplete="off"> Ex-Coregroupers
										</label><br>
									</div>
									<div class="col-md-2" aria-hidden="true" data-toggle="buttons">
										<label class="btn btn-primary check"> <input type="checkbox"
											checked autocomplete="off"> Parents/Guardians
										</label><br>
									</div>
								</div>
								<div class="row">
									<br> <label>Subject:</label>
									<textarea class="form-control" rows="1" cols="">Feature coming soon!</textarea>
									<br> <label>Message:</label>
									<textarea class="form-control" rows="6" cols="">Feature coming soon!</textarea>
									<br>

								</div>
								<div class="row">
									<div class="col-md-6 col-xs-6" align="left"></div>
									<div class="col-md-6 col-xs-6" align="right">
										<a class="btn btn-success"><i class="fa fa-envelope-o fa-lg"></i>
											Send</a>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- /.row -->
	</div>
	<!-- /.container -->
	<?php
	include_once ("common_files/javascript.php");
	?>
</body>
</html>
