<?php
require_once ("BaseLogicClass.php");
class Members {
	private $connection;
	public function __construct() {
		$BaseLogicClass = new BaseLogicClass ();
		$db = $BaseLogicClass->connect ();
		$this->connection = $db;
	}
	public function execute_query($sql) {
		$query = $this->connection->prepare ( $sql );
		return $query;
	}
	public function register($name, $date_of_birth, $gender, $school, $church, $career, $allergy, $address, $email, $phone, $other_phone, $parent_name, $parent_phone, $parent_career, $parent_email, $level, $group_name) {
		try {
			$query = $this->connection->prepare ( "INSERT INTO members(name,date_of_birth,gender,school,church,career,allergy,address,email,phone,other_phone,parent_name,parent_phone,parent_career,parent_email,level,group_name) VALUES(:name,:date_of_birth,:gender,:school,:church,:career,:allergy,:address,:email,:phone,:other_phone,:parent_name,:parent_phone,:parent_career,:parent_email,:level,:group_name)" );
			
			$query->bindParam ( ":name", $name );
			$query->bindParam ( ":date_of_birth", $date_of_birth );
			$query->bindParam ( ":gender", $gender );
			$query->bindParam ( ":school", $school );
			$query->bindParam ( ":church", $church );
			$query->bindParam ( ":career", $career );
			$query->bindParam ( ":allergy", $allergy );
			$query->bindParam ( ":address", $address );
			$query->bindParam ( ":email", $email );
			$query->bindParam ( ":phone", $phone );
			$query->bindParam ( ":other_phone", $other_phone );
			$query->bindParam ( ":parent_name", $parent_name );
			$query->bindParam ( ":parent_phone", $parent_phone );
			$query->bindParam ( ":parent_career", $parent_career );
			$query->bindParam ( ":parent_email", $parent_email );
			$query->bindParam ( ":level", $level );
			$query->bindParam ( ":group_name", $group_name );
			
			$query->execute ();
			
			return $query;
		} catch ( PDOException $e ) {
			echo $e->getMessage ();
		}
	}
	public function getList() {
		try {
			$query = $this->connection->prepare ( "SELECT * FROM members WHERE 1=1 ORDER BY id" );
			$query->execute ();
			while ( $row = $query->fetch ( PDO::FETCH_ASSOC ) ) {
				$data [] = $row;
			}
			return $data;
		} catch ( PDOException $e ) {
			echo $e->getMessage ();
		}
	}
	public function delete($id) {
		try {
			$query = $this->connection->prepare ( "DELETE FROM members WHERE id=" . $id );
			$query->execute ();
			$count = $query->rowCount ();
			return $count;
		} catch ( PDOException $e ) {
			echo $e->getMessage ();
		}
	}
	public function getGroups($level) {
		try {
			$query = $this->connection->prepare ( "SELECT name from groups WHERE level=$level" );
			$query->execute ();
			while ( $row = $query->fetch ( PDO::FETCH_ASSOC ) ) {
				$fill_groups [] = $row;
			}
			return $fill_groups;
		} catch ( PDOException $e ) {
			echo $e->getMessage ();
		}
	}
	public function is_Loggedin() {
		if (isset ( $_SESSION ['user_session'] )) {
			return true;
		}
	}
	public function redirect($url) {
		header ( "Location:$url" );
	}
	public function logout() {
		session_destroy ();
		unset ( $_SESSION ['user_session'] );
		return true;
	}
}
?>