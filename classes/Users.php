<?php
require_once ("BaseLogicClass.php");
class Users {
	private $connection;
	public function __construct() {
		$BaseLogicClass = new BaseLogicClass ();
		$db = $BaseLogicClass->connect ();
		$this->connection = $db;
	}
	public function execute_query($sql) {
		$query = $this->connection->prepare ( $sql );
		return $query;
	}
	public function register($username, $email, $password, $added_by) {
		try {
			$encrypted_password = password_hash ( $password, PASSWORD_DEFAULT );
			
			$query = $this->connection->prepare ( "INSERT INTO users(username,email,password,added_by) VALUES(:username,:email,:password,:added_by)" );
			
			$query->bindParam ( ":username", $username );
			$query->bindParam ( ":email", $email );
			$query->bindParam ( ":password", $encrypted_password );
			$query->bindParam ( ":added_by", $added_by );
			
			$query->execute ();
			
			return $query;
		} catch ( PDOException $e ) {
			echo $e->getMessage ();
		}
	}
	public function getList() {
		try {
			$query = $this->connection->prepare ( "SELECT * FROM users WHERE 1=1 ORDER BY id" );
			$query->execute ();
			while ( $row = $query->fetch ( PDO::FETCH_ASSOC ) ) {
				$data [] = $row;
			}
			return $data;
		} catch ( PDOException $e ) {
			echo $e->getMessage ();
		}
	}
	public function login($username, $email, $password) {
		try {
			$query = $this->connection->prepare ( "SELECT * FROM users WHERE username=:username OR email=:email" );
			$query->execute ( array (
					'username' => $username,
					'email' => $email 
			) );
			$userRow = $query->fetch ( PDO::FETCH_ASSOC );
			if ($query->rowCount () == 1) {
				if (password_verify ( $password, $userRow ['password'] )) {
					$_SESSION ['user_session'] = $userRow ['username'];
					return true;
				} else {
					return false;
				}
			}
		} catch ( PDOException $e ) {
			echo $e->getMessage ();
		}
	}
	public function is_Loggedin() {
		if (isset ( $_SESSION ['user_session'] )) {
			return true;
		}
	}
	public function redirect($url) {
		header ( "Location:$url" );
	}
	public function logout() {
		session_destroy ();
		unset ( $_SESSION ['user_session'] );
		return true;
	}
}
?>