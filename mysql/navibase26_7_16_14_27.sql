-- phpMyAdmin SQL Dump
-- version 4.6.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 26, 2016 at 11:27 AM
-- Server version: 5.7.9
-- PHP Version: 5.6.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `navibase`
--

-- --------------------------------------------------------

--
-- Table structure for table `attendance`
--

DROP TABLE IF EXISTS `attendance`;
CREATE TABLE IF NOT EXISTS `attendance` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `date` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

DROP TABLE IF EXISTS `groups`;
CREATE TABLE IF NOT EXISTS `groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `level` varchar(255) NOT NULL,
  `overseer` varchar(255) NOT NULL,
  `year` year(4) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `group_name` (`name`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `name`, `description`, `level`, `overseer`, `year`) VALUES
(1, 'Test', 'This is a test Group', 'Expo', 'Overseer 1', 2009),
(2, 'Test2', 'Test', 'Expo', 'Over', 2009);

-- --------------------------------------------------------

--
-- Table structure for table `levels`
--

DROP TABLE IF EXISTS `levels`;
CREATE TABLE IF NOT EXISTS `levels` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `levels`
--

INSERT INTO `levels` (`id`, `name`, `description`) VALUES
(1, 'Exposition', 'The first level in Coregroup'),
(2, 'Coregroup', 'This is the next Level after Exposition');

-- --------------------------------------------------------

--
-- Table structure for table `members`
--

DROP TABLE IF EXISTS `members`;
CREATE TABLE IF NOT EXISTS `members` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `date_of_birth` varchar(255) NOT NULL,
  `gender` varchar(255) NOT NULL,
  `school` varchar(255) NOT NULL,
  `church` varchar(255) NOT NULL,
  `career` varchar(255) NOT NULL,
  `allergy` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `other_phone` varchar(255) DEFAULT NULL,
  `date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `parent_name` varchar(255) NOT NULL,
  `parent_phone` varchar(255) NOT NULL,
  `parent_career` varchar(255) NOT NULL,
  `parent_email` varchar(255) NOT NULL,
  `level` varchar(255) NOT NULL,
  `group_name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `members`
--

INSERT INTO `members` (`id`, `name`, `date_of_birth`, `gender`, `school`, `church`, `career`, `allergy`, `address`, `email`, `phone`, `other_phone`, `date`, `parent_name`, `parent_phone`, `parent_career`, `parent_email`, `level`, `group_name`) VALUES
(1, 'Michael', '21/03/1994', 'Male', 'St. Mary\'s School, Yala', 'CITAM Valley Road', 'Programmer', 'NIL', '21970 Nairobi', 'odindo.michael@gmail.com', '0706664400', '0710329166', '2016-06-01 16:06:00', 'Edward Teddy', '0721294196', 'Director', 'todindo@gmail.com', 'Operation Timothy', 'OT'),
(4, 'Test Subject', '1989-05-21', 'Female', 'Testable School', 'Test Church', 'Test Career', 'Testy', '21589', 'testsub@gmail.com', '0721051989', '0721051989', '2016-07-25 09:45:05', 'Test Parent', '0721051989', 'Test Man', 'testparent@hotmail.com', 'Exposition', 'Test');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `added_by` varchar(255) NOT NULL,
  `date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `email`, `password`, `added_by`, `date`) VALUES
(1, 'amiro', 'amiro@gmail.com', '$2a$08$ZLnoUYqvCAXQdRwUbJ59duOmxBVM4ng4F3i3dnQ.dLZl068xdtQS.', '', '2016-07-23 09:00:35'),
(2, 'frankmwangi', 'phrankmwangi@gmail.com', '$2y$10$nUpYzeWb/XGuoTLMCXCn..pzHvuJF97Q8i56dbJ404SlYJG2sQAdO', '', '2016-07-23 10:12:05'),
(3, 'jondera', 'joshuaondera@gmail.com', '$2y$10$KYHzWaJ/aiicMad3EXcNF.vTHtvE854NgnErSJaE10aSggOQPOHFu', '', '2016-07-23 16:19:37'),
(4, 'NewUser', 'newUser@gmail.com', '$2y$10$qIJRIV6W3MoqSOaVaXg.2uZ/lj6a09yG0Z0QsfHs5.quXWnaxFRLe', '', '2016-07-23 18:31:05'),
(5, 'Testing', 'testingregistrar@gmail.com', '$2y$10$Jn34vdLUvo8YTsTrv86ITuugr9e18HMYV5bboKGsrt6zEPSDkBxNK', 'amiro', '2016-07-26 10:08:03');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
